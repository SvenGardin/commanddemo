/**
 *
 */
package com.cgi.osd.commanddemo;

/**
 * This interface defines the possible directions for the Trailblazer.
 *
 */
public interface Trailblazer {

    public void down();

    public void left();

    public void right();

    public void up();
}
