package com.cgi.osd.commanddemo;

/**
 * This class is responsible for the operations that the Trailblazer can perform.
 *
 *
 */
public class TrailblazerImpl implements Trailblazer {

    @Override
    public void down() {
	System.out.println("One step down.");
    }

    @Override
    public void left() {
	System.out.println("One step left.");
    }

    @Override
    public void right() {
	System.out.println("One step right.");
    }

    @Override
    public void up() {
	System.out.println("One step up.");
    }

}
