package com.cgi.osd.commanddemo;

/**
 * This is the start point of the Command Demo application
 *
 */
public class Main {

    public static void main(String[] args) {
	final CommandLineInterpreter commandLineInterpreter = new CommandLineInterpreter();

	while (!commandLineInterpreter.isStop()) {
	    commandLineInterpreter.dispatchUserInput();
	}
    }

}
