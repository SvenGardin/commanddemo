package com.cgi.osd.commanddemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.cgi.osd.commanddemo.command.DownCommand;
import com.cgi.osd.commanddemo.command.LeftCommand;
import com.cgi.osd.commanddemo.command.RightCommand;
import com.cgi.osd.commanddemo.command.UpCommand;

/**
 * This class is responsible for interpreting the user's input.
 *
 */
public class CommandLineInterpreter {

    private boolean stop = false;

    public void dispatchUserInput() {
	try {
	    final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	    System.out.print(">");
	    final String input = reader.readLine();
	    switch (input) {
	    case "exit":
		setStop(true);
		break;
	    case "u":
		CommandProcessor.getInstance().runCommand(new UpCommand());
		break;
	    case "d":
		CommandProcessor.getInstance().runCommand(new DownCommand());
		break;
	    case "l":
		CommandProcessor.getInstance().runCommand(new LeftCommand());
		break;
	    case "r":
		CommandProcessor.getInstance().runCommand(new RightCommand());
		break;
	    case "undo":
		CommandProcessor.getInstance().undoLatestCommand();
		break;
	    case "uall":
		CommandProcessor.getInstance().undoAllCommands();
		break;
	    default:
		System.out.println("Valid commands are u | d | l | r | undo | uall | exit");
		break;
	    }
	} catch (final IOException e) {
	    System.err.println("Failed to read user's input: " + e.getMessage());
	}
    }

    public boolean isStop() {
	return this.stop;
    }

    public void setStop(boolean stop) {
	this.stop = stop;
    }

}
