package com.cgi.osd.commanddemo;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.cgi.osd.commanddemo.command.Command;

/**
 * This class is responsible for keeping track of the command execution and in that way make undo possible.
 *
 * Note that the class is implemented as a non-thread safe Singleton.
 *
 */
public class CommandProcessor {

    private static class CommandProcessorHolder {
	private static final CommandProcessor INSTANCE = new CommandProcessor();
    }

    public static CommandProcessor getInstance() {
	return CommandProcessorHolder.INSTANCE;
    }

    private final List<Command> commands = new LinkedList<>();

    private CommandProcessor() {
    }

    public void runCommand(Command command) {
	command.execute();
	this.commands.add(command);
    }

    public void undoAllCommands() {
	final ListIterator<Command> iterator = this.commands.listIterator(this.commands.size());
	while (iterator.hasPrevious()) {
	    iterator.previous().undo();
	    iterator.remove();
	}
    }

    public void undoLatestCommand() {
	final ListIterator<Command> iterator = this.commands.listIterator(this.commands.size());
	if (iterator.hasPrevious()) {
	    iterator.previous().undo();
	    iterator.remove();
	}
    }

}
