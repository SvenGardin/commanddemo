/**
 *
 */
package com.cgi.osd.commanddemo.command;

/**
 * This interface defines the methods for the Command.
 *
 */
public interface Command {

    public void execute();

    public void undo();

}
