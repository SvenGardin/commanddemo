package com.cgi.osd.commanddemo.command;

public class UpCommand extends CommandBase implements Command {

    @Override
    public void execute() {
	this.trailBlazer.up();
    }

    @Override
    public void undo() {
	this.trailBlazer.down();
    }

}
