package com.cgi.osd.commanddemo.command;

public class LeftCommand extends CommandBase implements Command {

    @Override
    public void execute() {
	this.trailBlazer.left();
    }

    @Override
    public void undo() {
	this.trailBlazer.right();
    }

}
