package com.cgi.osd.commanddemo.command;

import com.cgi.osd.commanddemo.Trailblazer;
import com.cgi.osd.commanddemo.TrailblazerImpl;

public abstract class CommandBase {

    protected final Trailblazer trailBlazer = new TrailblazerImpl();

}
