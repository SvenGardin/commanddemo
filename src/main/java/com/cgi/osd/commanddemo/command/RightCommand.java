package com.cgi.osd.commanddemo.command;

public class RightCommand extends CommandBase implements Command {

    @Override
    public void execute() {
	this.trailBlazer.right();
    }

    @Override
    public void undo() {
	this.trailBlazer.left();
    }

}
