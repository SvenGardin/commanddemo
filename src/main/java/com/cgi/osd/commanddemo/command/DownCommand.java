package com.cgi.osd.commanddemo.command;

public class DownCommand extends CommandBase implements Command {

    @Override
    public void execute() {
	this.trailBlazer.down();
    }

    @Override
    public void undo() {
	this.trailBlazer.up();
    }

}
